  Ordered List
  1. test
  1. test 1
  1. test 2
    
  Unordered List
  * Bullet 1
    * Subbullet 1
    * Subbullet 2
  * Bullet 2
  * Bullet 3
  
- Dashes work just as well
- And if you have sub points, put two spaces before the dash or star:
  - Like this
  - And this
  
  
| A simple | table |
| with multiple | lines|


term
: definition
: another definition

another term
and another term
: and a definition for the term

| Default aligned | Left aligned | Center aligned  | Right aligned  |
|-----------------|:-------------|:---------------:|---------------:|
| First body part | Second cell  | Third cell      | fourth cell    |
| Second line     | foo          | **strong**      | baz            |
| Third line      | quux         | baz             | bar            |
|-----------------+--------------+-----------------+----------------|
| Second body     |              |                 |                |
| 2nd line        |              |                 |                |
|-----------------+--------------+-----------------+----------------|
| Third body      |              |                 | Foo            |
{: .custom-class #custom-id}



| Header1 | Header2 | Header3 |
|:--------|:-------:|--------:|
| cell1   | cell2   | cell3   |
| cell4   | cell5   | cell6   |
|----
| cell1   | cell2   | cell3   |
| cell4   | cell5   | cell6   |
|=====
| Foot1   | Foot2   | Foot3
{: rules="groups"}

~~~ ruby
def what?
  42
end
~~~
